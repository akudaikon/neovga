LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY neoControllerPSX IS
  GENERIC
  (
    sys_clk         : INTEGER := 50_000_000;        -- System clock speed in Hz
    psx_clk         : INTEGER := 100_000;           -- PSX clock speed in Hz
    poll_time       : INTEGER := 1000               -- Controller poll time (ms/100)
  );

  PORT
  (
    clk             : IN    STD_LOGIC;              -- System clock input
    reset_n         : IN    STD_LOGIC;              -- Reset (active low)

    psx1attn        : OUT   STD_LOGIC;              -- PSX controller 1 attention output
    psx2attn        : OUT   STD_LOGIC;              -- PSX controller 2 attention output
    psxclk          : OUT   STD_LOGIC;              -- PSX clock output
    psxcmd          : OUT   STD_LOGIC;              -- PSX commmand output
    psxdata         : IN    STD_LOGIC;              -- PSX data input
    psx1connected   : OUT   STD_LOGIC;              -- PSX controller 1 connected LED output
    psx2connected   : OUT   STD_LOGIC;              -- PSX controller 2 connected LED output
    psx2power       : OUT   STD_LOGIC;              -- PSX controller 2 power
   
    P1_up           : OUT   STD_LOGIC;              -- Neo Geo controller 1 joystick up output
    P1_down         : OUT   STD_LOGIC;              -- Neo Geo controller 1 joystick down output
    P1_left         : OUT   STD_LOGIC;              -- Neo Geo controller 1 joystick left output
    P1_right        : OUT   STD_LOGIC;              -- Neo Geo controller 1 joystick right output
    P1_a            : OUT   STD_LOGIC;              -- Neo Geo controller 1 A button output
    P1_b            : OUT   STD_LOGIC;              -- Neo Geo controller 1 B button output
    P1_c            : OUT   STD_LOGIC;              -- Neo Geo controller 1 C button output
    P1_d            : OUT   STD_LOGIC;              -- Neo Geo controller 1 D button output
    P1_start        : OUT   STD_LOGIC;              -- Neo Geo controller 1 start button output
    P1_select       : OUT   STD_LOGIC;              -- Neo Geo controller 1 select button output

    P2_up           : OUT   STD_LOGIC;              -- Neo Geo controller 2 joystick up output
    P2_down         : OUT   STD_LOGIC;              -- Neo Geo controller 2 joystick down output
    P2_left         : OUT   STD_LOGIC;              -- Neo Geo controller 2 joystick left output
    P2_right        : OUT   STD_LOGIC;              -- Neo Geo controller 2 joystick right output
    P2_a            : OUT   STD_LOGIC;              -- Neo Geo controller 2 A button output
    P2_b            : OUT   STD_LOGIC;              -- Neo Geo controller 2 B button output
    P2_c            : OUT   STD_LOGIC;              -- Neo Geo controller 2 C button output
    P2_d            : OUT   STD_LOGIC;              -- Neo Geo controller 2 D button output
    P2_start        : OUT   STD_LOGIC;              -- Neo Geo controller 2 start button output
    P2_select       : OUT   STD_LOGIC               -- Neo Geo controller 2 select button output
  );
END neoControllerPSX;

ARCHITECTURE logic OF neoControllerPSX IS
  TYPE data_array         IS array(8 downto 0) of STD_LOGIC_VECTOR(7 DOWNTO 0);
  TYPE machine            IS(init, startcmd, cmd, latchout, pause);
  TYPE machine2           IS(poll, enterconfig, setanalog, exitconfig);

  CONSTANT divider        : INTEGER := (sys_clk / psx_clk) / 2;   -- Number of clocks in 1/2 cycle of PSX clk

  SIGNAL state            : machine;                              -- PSX controller communication state machine
  SIGNAL mode             : machine2;

  SIGNAL clk_gen          : STD_LOGIC;                            -- PSX clock gen
  SIGNAL clk_gen_prev     : STD_LOGIC;                            -- PSX clock gen previous state

  SIGNAL bit_cnt          : INTEGER RANGE 0 TO 7 := 7;            -- Tracks bit number in transaction
  SIGNAL byte_cnt         : INTEGER RANGE 0 TO 8 := 8;            -- Tracks byte number in transaction

  SIGNAL cmd_byte         : data_array;                           -- Holds command bytes to be sent (5 bytes)
  SIGNAL controller_byte  : data_array;                           -- Holds controller data (5 bytes)
  SIGNAL cur_controller   : STD_LOGIC;                            -- Holds current controller to poll (0 = controller 1, 1 = controller 2)
  SIGNAL psx1active       : STD_LOGIC;
  SIGNAL psx2active       : STD_LOGIC;

  BEGIN
    -- Generate the timing for the PSX clock
    PROCESS(clk, reset_n)
      VARIABLE count      : INTEGER RANGE 0 TO divider * 2;       -- Timing for clock generation

    BEGIN
      IF (reset_n = '0') THEN                                     -- Reset asserted
        count := 0;                                               -- Reset timer
        clk_gen <= '0';

      ELSIF (clk'EVENT AND clk = '1') THEN
        clk_gen_prev <= clk_gen;                                  -- Store previous state of clock
        count := count + 1;                                       -- Increment timer

        IF (count = divider * 2 - 1) THEN                         -- End of timing cycle
          count := 0;                                             -- Reset timer
        END IF;

        CASE count IS
          WHEN 0 TO divider - 1 =>                                -- First half of clock cycle
            clk_gen <= '1';                                       -- PSX clock goes high
          WHEN OTHERS =>                                          -- Second half of clock cycle
            clk_gen <= '0';                                       -- PSX clock goes low
        END CASE;
      END IF;
    END PROCESS;

    -- PSX Controller Communication
    PROCESS(clk, reset_n)
      VARIABLE pause_time : INTEGER RANGE 0 to poll_time;

    BEGIN
      IF (reset_n = '0') THEN                                     -- RESET STATE
        byte_cnt <= 8;                                            -- Reset data byte counter
        bit_cnt <= 7;                                             -- Reset data bit counter
        cur_controller <= '0';                                    -- Set current controller to controller 1
        psx1attn <= '1';                                          -- Set PSX controller 1 attention output high
        psx2attn <= '1';                                          -- Set PSX controller 2 attention output high
        psxcmd <= '1';                                            -- Set PSX command output high
        psx1active <= '0';                                        -- Set PSX controller 1 connected signal low
        psx2active <= '0';                                        -- Set PSX controller 2 connected signal low
        state <= init;                                            -- Go to init state
        mode <= poll;                                             -- Set initial mode to polling

      ELSIF (clk'EVENT AND clk = '1') THEN
        IF (clk_gen = '0' AND clk_gen_prev = '1') THEN            -- PSX clock rising edge
          CASE state IS
            WHEN init =>                                          -- INIT STATE
              byte_cnt <= 8;                                      -- Reset data byte counter
              bit_cnt <= 7;                                       -- Reset data bit counter
              state <= startcmd;                                  -- Go to start command send state

              CASE mode IS
                -- All commands are LSB first because I'm lazy!
                WHEN poll =>                                      -- Poll command
                  cmd_byte(8) <= b"10000000";                     -- Command 1 = 0x01
                  cmd_byte(7) <= b"01000010";                     -- Command 2 = 0x42
                  cmd_byte(6) <= b"11111111";                     -- Command 3 = 0xFF
                  cmd_byte(5) <= b"11111111";                     -- Command 4 = 0xFF
                  cmd_byte(4) <= b"11111111";                     -- Command 5 = 0xFF
                  cmd_byte(3) <= b"11111111";                     -- Command 6 = 0xFF
                  cmd_byte(2) <= b"11111111";                     -- Command 7 = 0xFF
                  cmd_byte(1) <= b"11111111";                     -- Command 8 = 0xFF
                  cmd_byte(0) <= b"11111111";                     -- Command 9 = 0xFF

                WHEN enterconfig =>                               -- Enter configuration mode command
                  cmd_byte(8) <= b"10000000";                     -- Command 1 = 0x01
                  cmd_byte(7) <= b"11000010";                     -- Command 2 = 0x43
                  cmd_byte(6) <= b"00000000";                     -- Command 3 = 0x00
                  cmd_byte(5) <= b"10000000";                     -- Command 4 = 0x01
                  cmd_byte(4) <= b"00000000";                     -- Command 5 = 0x00
                  cmd_byte(3) <= b"00000000";                     -- Command 6 = 0x00
                  cmd_byte(2) <= b"00000000";                     -- Command 7 = 0x00
                  cmd_byte(1) <= b"00000000";                     -- Command 8 = 0x00
                  cmd_byte(0) <= b"00000000";                     -- Command 9 = 0x00

                WHEN setanalog =>                                 -- Set analog mode command
                  cmd_byte(8) <= b"10000000";                     -- Command 1 = 0x01
                  cmd_byte(7) <= b"00100010";                     -- Command 2 = 0x44
                  cmd_byte(6) <= b"00000000";                     -- Command 3 = 0x00
                  cmd_byte(5) <= b"10000000";                     -- Command 4 = 0x01
                  cmd_byte(4) <= b"11000000";                     -- Command 5 = 0x03
                  cmd_byte(3) <= b"00000000";                     -- Command 6 = 0x00
                  cmd_byte(2) <= b"00000000";                     -- Command 7 = 0x00
                  cmd_byte(1) <= b"00000000";                     -- Command 8 = 0x00
                  cmd_byte(0) <= b"00000000";                     -- Command 9 = 0x00

                WHEN exitconfig =>                                -- Exit configuration mode command
                  cmd_byte(8) <= b"10000000";                     -- Command 1 = 0x01
                  cmd_byte(7) <= b"11000010";                     -- Command 2 = 0x43
                  cmd_byte(6) <= b"00000000";                     -- Command 3 = 0x00
                  cmd_byte(5) <= b"00000000";                     -- Command 4 = 0x00
                  cmd_byte(4) <= b"01011010";                     -- Command 5 = 0x5A
                  cmd_byte(3) <= b"01011010";                     -- Command 6 = 0x5A
                  cmd_byte(2) <= b"01011010";                     -- Command 7 = 0x5A
                  cmd_byte(1) <= b"01011010";                     -- Command 8 = 0x5A
                  cmd_byte(0) <= b"01011010";                     -- Command 9 = 0x5A
              END CASE;
              
              IF (cur_controller = '0') THEN
                psx1attn <= '0';                                  -- PSX controller 1 attention line goes low
              ELSE
                psx2attn <= '0';                                  -- PSX controller 2 attention line goes low
              END IF;

            WHEN startcmd =>                                      -- COMMAND START STATE
              psxcmd <= cmd_byte(byte_cnt)(bit_cnt);              -- Output first bit of command byte on PSX command line
              state <= cmd;

            WHEN cmd =>                                           -- COMMAND STATE
              IF (bit_cnt = 0) THEN                               -- If we've sent all 8 bits...
                IF (byte_cnt = 0) THEN                            -- If we've sent all command bytes...
                  psxcmd <= '1';                                  -- PSX command output goes high again
                  state <= latchout;                              -- Go to controller latch out state
                  psx1attn <= '1';                                -- PSX controller 1 attention line goes high
                  psx2attn <= '1';                                -- PSX controller 2 attention line goes high
                ELSE
                  bit_cnt <= 7;                                   -- Reset bit counter for next time
                  byte_cnt <= byte_cnt - 1;                       -- Decrement byte counter
                  state <= startcmd;                              -- Go to start command send state for next command byte
                END IF;
              ELSE                                                -- Else output next bit...
                bit_cnt <= bit_cnt - 1;                           -- Decrement bit counter
                psxcmd <= cmd_byte(byte_cnt)(bit_cnt - 1);        -- Output next bit of command byte on PSX command line
                state <= cmd;                                     -- Loop current state
              END IF;

            WHEN latchout =>                                      -- CONTROLLER DATA OUTPUT LATCH STATE
              CASE mode IS
                WHEN poll =>
                  if (controller_byte(7) = b"10000010") THEN      -- If controller is in digital mode (0x41), need to switch to analog
                    mode <= enterconfig;                          -- Set mode to enter config
                    state <= init;                                -- Go to init state
                  ELSE
                    pause_time := 0;                              -- Reset pause time counter
                    cur_controller <= NOT cur_controller;         -- Switch to poll other PSX controller

                    -- Byte and bit order are reversed since byte_cnt and bit_cnt are count down
                    -- Both PSX and Neo Geo controllers are active low
                    IF (cur_controller = '0') THEN                -- Neo Geo controller 1 output
                      P1_up <= controller_byte(5)(3);
                      P1_down <= controller_byte(5)(1);
                      P1_left <= controller_byte(5)(0);
                      P1_right <= controller_byte(5)(2);
                      P1_a <= controller_byte(4)(3);              -- Triangle
                      P1_b <= controller_byte(4)(2);              -- Circle
                      P1_c <= controller_byte(4)(1);              -- X
                      P1_d <= controller_byte(4)(0);              -- Square
                      P1_start <= controller_byte(5)(4);
                      P1_select <= controller_byte(5)(7);
                      state <= init;                              -- Go to init state to poll PSX controller 2

                      -- If controller_byte(3) = 0xFF, controller is connected
                      IF (controller_byte(3) = b"11111111") THEN
                        psx1active <= '1';
                      ELSE
                        psx1active <= '0';
                      END IF;

                    ELSE                                          -- Neo Geo controller 2 output
                      P2_up <= controller_byte(5)(3);
                      P2_down <= controller_byte(5)(1);
                      P2_left <= controller_byte(5)(0);
                      P2_right <= controller_byte(5)(2);
                      P2_a <= controller_byte(4)(3);
                      P2_b <= controller_byte(4)(2);
                      P2_c <= controller_byte(4)(1);
                      P2_d <= controller_byte(4)(0);
                      P2_start <= controller_byte(5)(4);
                      P2_select <= controller_byte(5)(7);
                      state <= pause;                             -- Go to pause state

                      -- If controller_byte(3) = 0xFF, controller connected
                      IF (controller_byte(3) = b"11111111") THEN
                        psx2active <= '1';
                      ELSE
                        psx2active <= '0';
                      END IF;
                    END IF;
                  END IF;

                WHEN enterconfig =>                               -- If we've entered configuration mode...
                  mode <= setanalog;                              -- Next mode is to set analog mode
                  state <= init;                                  -- Go back to init state

                WHEN setanalog =>                                 -- If we've set analog mode...
                  mode <= exitconfig;                             -- Next mode is to exit configuration mode
                  state <= init;                                  -- Go back to init state

                WHEN exitconfig =>                                -- If we've exitied configuration mode...
                  mode <= poll;                                   -- We're done, so start polling again
                  state <= init;                                  -- Go back to init state
              END CASE;

            WHEN pause =>                                         -- PAUSE STATE
              pause_time := pause_time + 1;                       -- Increment pause counter

              IF (pause_time = poll_time) THEN                    -- Wait for poll_time before polling again
                state <= init;                                    -- Start next controller poll
              END IF;

            WHEN OTHERS =>
              state <= init;
          END CASE;

        -- PSX data read on falling edge
        ELSIF (clk_gen = '1' AND clk_gen_prev = '0') THEN         -- PSX clock falling edge
          CASE state IS
             WHEN cmd =>
               controller_byte(byte_cnt)(bit_cnt) <= psxdata;     -- Read in PSX data bit
             WHEN OTHERS => NULL;
          END CASE;
        END IF;
      END IF;
  END PROCESS;

  -- Output PSX clock only on certain states
  WITH state SELECT psxclk <= clk_gen_prev WHEN cmd,
                              '1'          WHEN OTHERS;
                    
  -- PSX controller 2 reciever only gets powered if controller 1 connected
  -- or if controller 2 is already connected
  psx2power <= (psx1active OR psx2active);
  
  -- PSX controller connected LED output
  psx1connected <= psx1active;
  psx2connected <= psx2active;

END logic;
