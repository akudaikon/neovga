LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY neoControllerWii IS
  GENERIC (
    sys_clk   : INTEGER := 50_000_000;  -- System clock speed in Hz
    i2c_clk   : INTEGER := 100_000;     -- I2C clock speed (SCL) will run at in Hz
    poll_time : INTEGER := 1000         -- Controller poll time
  );
    
  PORT (
    clk       : IN  STD_LOGIC;        -- System clock input
    reset_n   : IN  STD_LOGIC;        -- Reset (active low)
    sda       : INOUT STD_LOGIC;      -- I2C serial data
    scl       : INOUT STD_LOGIC;      -- I2C serial clock
    
    bup       : OUT STD_LOGIC;        -- Joystick up output
    bdown     : OUT STD_LOGIC;        -- Joystick down output
    bleft     : OUT STD_LOGIC;        -- Joystick left output
    bright    : OUT STD_LOGIC;        -- Joystick right output
    ba        : OUT STD_LOGIC;        -- A button output
    bb        : OUT STD_LOGIC;        -- B button output
    bc        : OUT STD_LOGIC;        -- C button output
    bd        : OUT STD_LOGIC;        -- D button output
    bstart    : OUT STD_LOGIC;        -- Start button output
    bselect   : OUT STD_LOGIC;        -- Select button output
    
    done      : OUT STD_LOGIC         -- Done signal to tell other neoController we're done and it can poll now
                                      -- Needed because both polling at the same time causes enough interference 
                                      -- to screw up the I2C transactions occasionally
                                      -- This signal goes into the other neoController's reset input
  );
END neoControllerWii;

ARCHITECTURE logic OF neoControllerWii IS
  CONSTANT divider  :  INTEGER := (sys_clk / i2c_clk) / 4; --number of clocks in 1/4 cycle of scl

  TYPE data_array IS array(5 downto 0) of STD_LOGIC_VECTOR(7 DOWNTO 0);
  TYPE machine IS(init, start, address, slv_ack1, wr, rd, slv_ack2, mstr_ack, stop, latch_out, pause);

  SIGNAL state          : machine;                        -- state machine
  SIGNAL data_clk       : STD_LOGIC;                      -- data clock for sda
  SIGNAL data_clk_prev  : STD_LOGIC;                      -- data clock during previous system clock
  SIGNAL scl_clk        : STD_LOGIC;                      -- constantly running internal scl
  SIGNAL scl_ena        : STD_LOGIC := '0';               -- enables internal scl to output
  SIGNAL sda_int        : STD_LOGIC := '1';               -- internal sda
  SIGNAL sda_ena_n      : STD_LOGIC;                      -- enables internal sda to output
  SIGNAL stretch        : STD_LOGIC := '0';               -- identifies if slave is stretching scl
  SIGNAL addr_rw        : STD_LOGIC_VECTOR(7 DOWNTO 0);   -- latched in address and read/write
  SIGNAL data_tx        : STD_LOGIC_VECTOR(7 DOWNTO 0);   -- latched in data to write to slave
  SIGNAL data_rx        : STD_LOGIC_VECTOR(7 DOWNTO 0);   -- data received from slave
  SIGNAL bit_cnt        : INTEGER RANGE 0 TO 7 := 7;      -- tracks bit number in transaction
  SIGNAL byte_cnt       : INTEGER RANGE 0 TO 5 := 5;      -- tracks byte number in transaction
  SIGNAL controllerByte : data_array;
BEGIN
  -- Generate the timing for the bus clock (scl_clk) and the data clock (data_clk)
  PROCESS(clk, reset_n)
    VARIABLE count  :  INTEGER RANGE 0 TO divider*4;  --timing for clock generation
  BEGIN
    IF (reset_n = '0') THEN               -- reset asserted
      stretch <= '0';
      count := 0;
    ELSIF (clk'EVENT AND clk = '1') THEN
      data_clk_prev <= data_clk;          -- store previous value of data clock
      IF (count = divider*4-1) THEN       -- end of timing cycle
        count := 0;                       -- reset timer
      ELSIF (stretch = '0') THEN          -- clock stretching from slave not detected
        count := count + 1;               -- continue clock generation timing
      END IF;
      CASE count IS
        WHEN 0 TO divider-1 =>            -- first 1/4 cycle of clocking
          scl_clk <= '0';
          data_clk <= '0';
        WHEN divider TO divider*2-1 =>    -- second 1/4 cycle of clocking
          scl_clk <= '0';
          data_clk <= '1';
        WHEN divider*2 TO divider*3-1 =>  -- third 1/4 cycle of clocking
          scl_clk <= '1';                 -- release scl
          IF (scl = '0') THEN             -- detect if slave is stretching clock
            stretch <= '1';
          ELSE
            stretch <= '0';
          END IF;
          data_clk <= '1';
        WHEN OTHERS =>                    -- last 1/4 cycle of clocking
          scl_clk <= '1';
          data_clk <= '0';
      END CASE;
    END IF;
  END PROCESS;

  -- state machine and writing to sda during scl low (data_clk rising edge)
  PROCESS(clk, reset_n)
    VARIABLE pause_time   : INTEGER RANGE 0 to poll_time;
    VARIABLE connect_delay  : INTEGER RANGE 0 to 20;  -- Wait a few transactions before latch_out
                                                      -- This is to avoid Wii calibration data being latched out
  BEGIN
  IF(reset_n = '0') THEN                  -- reset asserted
    state <= init;                        -- return to initial state
    scl_ena <= '0';                       -- sets scl high impedance
    sda_int <= '1';                       -- sets sda high impedance
    bit_cnt <= 7;                         -- restarts data bit counter
    byte_cnt <= 5;
    done <= '1';

   ELSIF(clk'EVENT AND clk = '1') THEN
    IF(data_clk = '1' AND data_clk_prev = '0') THEN  --data clock rising edge
      CASE state IS
        WHEN init =>                        -- INIT STATE
          addr_rw <= "10100100";            -- I2C Slave 0x52, write command
          data_tx <= "00000000";            -- I2C data to write (0x00)
          bit_cnt <= 7;
          done <= '0';                      -- Clear done flag, we're going to poll again
          state <= start;

        WHEN start =>                       -- START BIT STATE
          sda_int <= addr_rw(bit_cnt);      -- Put first bit of address on SDA
          state <= address;

        WHEN address =>                     -- OUTPUT ADDRESS STATE     
          IF(bit_cnt = 0) THEN              -- If we've transferred all 8 bits...
            sda_int <= '1';                 -- Release SDA for slave acknowledge
            bit_cnt <= 7;                   -- Reset bit counter for next time
            state <= slv_ack1;              -- Go to slave ack (address) state
          ELSE                              -- Else next clock cycle of address output
            bit_cnt <= bit_cnt - 1;         -- Decrement bit counter
            sda_int <= addr_rw(bit_cnt-1);  -- Output next bit of address to SDA
            state <= address;               -- Loop current state
          END IF;

        WHEN slv_ack1 =>                    -- SLAVE ACK STATE (ADDRESS)
          IF(addr_rw(0) = '0') THEN         -- If next is write command...
            sda_int <= data_tx(bit_cnt);    -- Put first bit of data on SDA
            state <= wr;                    -- Go to write state
          ELSE                              -- If next is read command...
            sda_int <= '1';                 -- Release SDA for incoming data
            byte_cnt <= 5;                  -- Set byte counter
            state <= rd;                    -- Go to read state
          END IF;

        WHEN wr =>                          -- WRITE STATE
          IF(bit_cnt = 0) THEN              -- If we've transferred all 8 bits...
            sda_int <= '1';                 -- Release SDA for slave acknowledge
            bit_cnt <= 7;                   -- Reset bit counter for next time
            state <= slv_ack2;              -- Go to slave ack (write) state
          ELSE                              -- Else next clock cycle of data output
            bit_cnt <= bit_cnt - 1;         -- Decrement bit counter
            sda_int <= data_tx(bit_cnt-1);  -- Output next bit of data to SDA
            state <= wr;                    -- Loop current state
          END IF;

        WHEN slv_ack2 =>                    -- SLAVE ACK STATE (WRITE)
          addr_rw <= "10100101";            -- Start read command - I2C Slave 0x52, read command
          data_tx <= "00000000";            -- I2C data to write (0x00) (not really needed)
          bit_cnt <= 7;                     -- Reset bit counter for next time
          sda_int <= addr_rw(bit_cnt);      -- Put first bit of address on SDA          
          state <= start;                   -- Start read transmission

        WHEN rd =>                          -- READ STATE
          IF(bit_cnt = 0) THEN              -- If we've read all 8 bits...
            IF(byte_cnt = 0) THEN           -- If we've read all 6 bytes...
              sda_int <= '1';               -- Send a no-acknowledge (for upcoming stop bit)
            ELSE                            -- Else not all bytes read yet...
              sda_int <= '0';               -- Send ack to show byte was received
            END IF;
            controllerByte(byte_cnt) <= data_rx;  -- Store received byte
            bit_cnt <= 7;                   -- Reset bit counter for next time
            state <= mstr_ack;              -- Go to master ack state
          ELSE                              -- Else next clock cycle of read state
            bit_cnt <= bit_cnt - 1;         -- Decrement bit counter
            state <= rd;                    -- Loop current state
          END IF;
 
        WHEN mstr_ack =>                    -- MASTER ACK STATE (AFTER READ)
          IF(byte_cnt = 0) THEN             -- If we've received all 6 bytes...
            state <= stop;                  -- Go to stop bit state
          ELSE                              -- Else read another byte...
            addr_rw <= "10100101";          -- I2C Slave 0x52, read command
            data_tx <= "00000000";          -- I2C data to write (0x00) (not really needed)
            byte_cnt <= byte_cnt - 1;       -- Decrement byte counter
            sda_int <= '1';                 -- Release SDA for incoming data
            state <= rd;                    -- Go to read state
          END IF;
          
        WHEN stop =>                        -- STOP BIT STATE
          state <= latch_out;               -- Go to controller data output latch state

        WHEN latch_out =>                   -- CONTROLLER DATA OUTPUT LATCH STATE
          done <= '1';                      -- Set done flag
          pause_time := 0;
          state <= pause;                   -- Go to pause state

          -- Check if all bits of byte 2 are low
          -- All bits in all bytes are high when controller not connected
          -- If not, we don't have a connected controller!
          IF (controllerByte(2) = "000000000") THEN
            IF (connect_delay = 10) THEN
              -- Byte order reversed since byte_cnt is count down
              -- Neo Geo controllers are active low
              bup <= NOT controllerByte(4)(5);
              bdown <= controllerByte(4)(4);
              bleft <= controllerByte(5)(4);
              bright <= NOT controllerByte(5)(5);
              ba <= controllerByte(0)(6);
              bb <= controllerByte(0)(4);
              bc <= controllerByte(0)(5);
              bd <= controllerByte(0)(3);
              bstart <= controllerByte(1)(2);
              bselect <= controllerByte(1)(4);
            ELSE
              connect_delay := connect_delay + 1;
            END IF;
          ELSE
            bup <= '1';
            bdown <= '1';
            bleft <= '1';
            bright <= '1';
            ba <= '1';
            bb <= '1';
            bc <= '1';
            bd <= '1';
            bstart <= '1';
            bselect <= '1';
            connect_delay := 0;
          END IF;

        WHEN pause =>                         -- PAUSE STATE
          pause_time := pause_time + 1;       -- Increment pause counter

          IF (pause_time = poll_time) THEN    -- Wait for poll_time before polling again
            state <= init;                    -- Start next controller poll
          END IF;

        WHEN OTHERS => NULL;
      END CASE;

      ELSIF(data_clk = '0' AND data_clk_prev = '1') THEN  --data clock falling edge
        CASE state IS
          WHEN start =>                  
            IF(scl_ena = '0') THEN            -- Starting new transaction
              scl_ena <= '1';                 -- Enable scl output
            END IF;

          WHEN rd =>                          -- Receiving slave data
            data_rx(bit_cnt) <= sda;          -- Receive current slave data bit
            
          WHEN stop =>                        -- Ending transmission
            scl_ena <= '0';                   -- Disable scl

          WHEN OTHERS => NULL;
        END CASE;
      END IF;
    END IF;
  END PROCESS;  

  -- SDA output enable logic
  WITH state SELECT
    sda_ena_n <= data_clk_prev WHEN start,      -- Generate start condition
                 NOT data_clk_prev WHEN stop,   -- Generate stop condition
                 sda_int WHEN OTHERS;           -- Set to internal SDA signal    

  -- SCL and SDA output logic
  scl <= '0' WHEN (scl_ena = '1' AND scl_clk = '0') ELSE 'Z';
  sda <= '0' WHEN sda_ena_n = '0' ELSE 'Z';
END logic;