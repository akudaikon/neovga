module neoVGA(neo_clk,      // 12 MHz clock input from Neo Geo
              neo_red,      // 5-bit red pixel input from Neo Geo
              neo_gre,      // 5-bit green pixel input from Neo Geo
              neo_blu,      // 5-bit blue pixel input from Neo Geo
              neo_sha,      // SHA input from Neo Geo
              neo_dak,      // DAK input from Neo Geo
              neo_sync,     // Video sync input from Neo Geo
              modeButton,   // Fake scanlines mode input button
              vga_red,      // 5-bit red pixel output to VGA
              vga_gre,      // 5-bit green pixel output to VGA
              vga_blu,      // 5-bit blue pixel output to VGA
              vga_hsync,    // Horizontal sync output to VGA
              vga_vsync     // Vertical sync output to VGA
              );

  parameter PIXELS_PER_LINE = 384;
  parameter LINES_PER_FRAME = 264;

  // VGA sync timing
  // Not quite to 640x480@60Hz VGA spec, but should (hopefully) be good enough
  parameter VGA_VISIBLE_START = 14;
  parameter VGA_VISIBLE_END = 334;
  parameter VGA_HSYNC_START = 338;
  parameter VGA_HSYNC_END = 382;
  parameter VGA_VSYNC_START = 8;
  parameter VGA_VSYNC_END = 10;

  // Input signals
  input wire neo_clk;
  input wire neo_sha, neo_dak, neo_sync;
  input [4:0] neo_red;
  input [4:0] neo_gre;
  input [4:0] neo_blu;
  input modeButton;

  // Output signals
  output wire [4:0] vga_red;
  output wire [4:0] vga_gre;
  output wire [4:0] vga_blu;
  output vga_hsync, vga_vsync;

  // Counters
  reg [9:0] neo_pixel = 0;
  reg [9:0] vga_pixel = 0;
  reg [9:0] cur_line = 0;
  reg [15:0] vsync_count = 0;
  reg active_buffer = 0;

  // Fake scanlines
  reg scanlineOn = 0;
  reg [1:0] scanlineMode = 0;

  // RGB pixel capture buffers
  reg [4:0] red_bufA [PIXELS_PER_LINE-1:0];
  reg [4:0] gre_bufA [PIXELS_PER_LINE-1:0];
  reg [4:0] blu_bufA [PIXELS_PER_LINE-1:0];
  reg [4:0] red_bufB [PIXELS_PER_LINE-1:0];
  reg [4:0] gre_bufB [PIXELS_PER_LINE-1:0];
  reg [4:0] blu_bufB [PIXELS_PER_LINE-1:0];

  // RGB output buffers
  reg [4:0] vga_red_buf = 0;
  reg [4:0] vga_gre_buf = 0;
  reg [4:0] vga_blu_buf = 0;

  // VGA syncing
  reg vga_hsync = 1;
  reg vga_vsync = 1;
  reg vga_blank = 0;

  // Only output pixels during active period
  assign vga_red = vga_blank ? 5'b00000 : vga_red_buf;
  assign vga_gre = vga_blank ? 5'b00000 : vga_gre_buf;
  assign vga_blu = vga_blank ? 5'b00000 : vga_blu_buf;

  // Generate 6Mhz clock from 12Mhz Neo Geo clock
  reg half_clk;
  always @(posedge neo_clk) begin
    half_clk <= ~half_clk;
  end

  // Input pixel counters (at 6MHz)
  always @(posedge half_clk) begin
    // Video syncing
    // Neo Geo Vsync is 8 lines long
    if (vsync_count == (PIXELS_PER_LINE * 8) - 1) begin
      neo_pixel <= 0;
      cur_line <= 0;
    end
    // If we've reached the end of an input line...
    else if (neo_pixel == PIXELS_PER_LINE - 1) begin
      // If we've reached the end of a frame...
      if (cur_line == LINES_PER_FRAME - 1) begin
        cur_line <= 0;              // reset the line counter
      end
      // Else increment line counter
      else begin
        cur_line <= cur_line + 1;
      end

      neo_pixel <= 0;             // reset input pixel counter
      active_buffer <= ~active_buffer;    // flip the active capture buffer
    end
    // Else increment input pixel counter
    else begin
      neo_pixel <= neo_pixel + 1;
    end
  end

  // Output pixel counters (at 12MHz)
  always @(posedge neo_clk) begin
    // Video syncing
    // Neo Geo Vsync is 8 lines long
    if (vsync_count == (PIXELS_PER_LINE * 8) - 1) begin
      vga_pixel <= 0;
      scanlineOn <= 0;
    end
    // If we've reached the end of an output line...
    else if (vga_pixel >= PIXELS_PER_LINE - 1) begin
      vga_pixel <= 0;         // reset output pixel counter
      scanlineOn <= ~scanlineOn;
    end
    // Else increment output pixel counter
    else begin
      vga_pixel <= vga_pixel + 1;
    end
  end

  // Count active Neo Geo Vsync length
  always @(posedge half_clk)
  begin
    if (neo_sync == 0) begin
      vsync_count <= vsync_count + 1;
    end else begin
      vsync_count <= 0;
    end
  end

  // VGA timing outputs (at 12Mhz)
  // Hsync and Vsync are active low for 640x480@60Hz VGA
  always @(posedge neo_clk) begin
    // Blanking period
    if (vga_pixel >= VGA_VISIBLE_START && vga_pixel < VGA_VISIBLE_END) begin
      vga_blank <= 0;
    end else begin
      vga_blank <= 1;
    end

    // Hsync
    if (vga_pixel >= VGA_HSYNC_START && vga_pixel < VGA_HSYNC_END) begin
      vga_hsync <= 0;
    end else begin
      vga_hsync <= 1;
    end

    // Vsync
    if (cur_line >= VGA_VSYNC_START && cur_line < VGA_VSYNC_END) begin
      vga_vsync <= 0;
    end else begin
      vga_vsync <= 1;
    end
  end

  // Capture a pixel to buffer (at 6Mhz)
  always @ (posedge half_clk) begin
    // Capture a pixel to active capture buffer
    if (!active_buffer) begin
      red_bufA[neo_pixel] <= neo_red;
      gre_bufA[neo_pixel] <= neo_gre;
      blu_bufA[neo_pixel] <= neo_blu;
    end
    else begin
      red_bufB[neo_pixel] <= neo_red;
      gre_bufB[neo_pixel] <= neo_gre;
      blu_bufB[neo_pixel] <= neo_blu;
    end

    // TODO: SHA and DAK - are these even needed?
  end

  // Output a pixel from active buffer (at 12MHz)
  // This will output a pixel at twice (12MHz) the capture speed (6MHz)
  always @(posedge neo_clk) begin
    // Buffer A
    if (active_buffer) begin
      if (scanlineOn) begin
        case(scanlineMode)
          // No scanline
          0: begin
            vga_red_buf <= red_bufA[vga_pixel];
            vga_gre_buf <= gre_bufA[vga_pixel];
            vga_blu_buf <= blu_bufA[vga_pixel];
          end
          // 25% scanline
          1: begin
            vga_red_buf <= red_bufA[vga_pixel] >> 1;
            vga_gre_buf <= gre_bufA[vga_pixel] >> 1;
            vga_blu_buf <= blu_bufA[vga_pixel] >> 1;
          end
          // 50% scanline
          2: begin
            vga_red_buf <= red_bufA[vga_pixel] >> 2;
            vga_gre_buf <= gre_bufA[vga_pixel] >> 2;
            vga_blu_buf <= blu_bufA[vga_pixel] >> 2;
          end
          // 100% scanline
          3: begin
            vga_red_buf <= 0;
            vga_gre_buf <= 0;
            vga_blu_buf <= 0;
          end
        endcase
      end
      else begin
        // No scanline for this line
        vga_red_buf <= red_bufA[vga_pixel];
        vga_gre_buf <= gre_bufA[vga_pixel];
        vga_blu_buf <= blu_bufA[vga_pixel];
      end
    end

    // Buffer B
    else begin
      if (scanlineOn) begin
        case(scanlineMode)
          // No scanline
          0: begin
            vga_red_buf <= red_bufB[vga_pixel];
            vga_gre_buf <= gre_bufB[vga_pixel];
            vga_blu_buf <= blu_bufB[vga_pixel];
          end
          // 25% scanline
          1: begin
            vga_red_buf <= red_bufB[vga_pixel] >> 1;
            vga_gre_buf <= gre_bufB[vga_pixel] >> 1;
            vga_blu_buf <= blu_bufB[vga_pixel] >> 1;
          end
          // 50% scanline
          2: begin
            vga_red_buf <= red_bufB[vga_pixel] >> 2;
            vga_gre_buf <= gre_bufB[vga_pixel] >> 2;
            vga_blu_buf <= blu_bufB[vga_pixel] >> 2;
          end
          // 100% scanline
          3: begin
            vga_red_buf <= 0;
            vga_gre_buf <= 0;
            vga_blu_buf <= 0;
          end
        endcase
      end
      else begin
        // No scanline for this line
        vga_red_buf <= red_bufB[vga_pixel];
        vga_gre_buf <= gre_bufB[vga_pixel];
        vga_blu_buf <= blu_bufB[vga_pixel];
      end
    end
  end

  // Switch to next fake scanlines output mode on button press (active low)
  reg[16:0] debounceCount;
  reg modeButton_0;
  always @(posedge half_clk) modeButton_0 <= modeButton;
  reg modeButton_1;
  always @(posedge half_clk) modeButton_1 <= modeButton_0;

  always @(posedge half_clk) begin
    if (!modeButton_1) begin
      if (debounceCount == 'h1ffff) begin
        scanlineMode <= scanlineMode + 1;
      end
      debounceCount <= 0;

    end else if (debounceCount != 'h1ffff) begin
      debounceCount <= debounceCount + 1;
    end
  end
endmodule
