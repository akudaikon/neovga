module neoGameDecoder(gamePosIn,
                      gamePosLEDs
                     );

  input [2:0] gamePosIn;
  output reg [3:0] gamePosLEDs;

  always @(gamePosIn)
  begin
    case (gamePosIn)
      3'b000  : gamePosLEDs = 4'b0001;
      3'b100  : gamePosLEDs = 4'b0010;
      3'b001  : gamePosLEDs = 4'b0100;
      3'b101  : gamePosLEDs = 4'b1000;
      default : gamePosLEDs = 4'b0000;
    endcase
  end
endmodule
